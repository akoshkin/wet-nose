<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    public function findForHomePage($filter): Paginator
    {
        $qb = $this->createQueryBuilder('a')
            ->where('a.status = :status')->setParameter('status', Article::STATUS_ACTIVE)
            ->orderBy('a.createdAt', $filter['sort'])
        ;

        if (null !== $filter['category']) {
            $qb
                ->leftJoin('a.category', 'c')->addSelect('c')
                ->andWhere(':category = a.category')->setParameter('category', $filter['category'])
            ;
        }

        return $this->paginate($qb->getQuery(), $filter['page'], Article::COUNT_ON_PAGE);
    }

    public function findForAdminPage($filter): Paginator
    {
        $qb = $this->createQueryBuilder('a')
            ->orderBy('a.createdAt', $filter['sort'])
        ;

        if (null !== $filter['category']) {
            $qb
                ->leftJoin('a.category', 'c')->addSelect('c')
                ->andWhere(':category = a.category')->setParameter('category', $filter['category'])
            ;
        }

        return $this->paginate($qb->getQuery(), $filter['page'], Article::ADMIN_COUNT_ON_PAGE);
    }

    public function paginate($dql, int $page, int $limit = Article::COUNT_ON_PAGE): Paginator
    {
        $paginator = new Paginator($dql);
        $paginator->getQuery()
            ->setFirstResult((int) ($limit * ($page - 1)) < 0 ? 0 : $limit * ($page - 1))
            ->setMaxResults($limit)
        ;

        return $paginator;
    }
}
