<?php

namespace App\Repository;

use App\Entity\Article;
use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Category::class);
    }

    public function findForAdminPage($filter): Paginator
    {
        $qb = $this->createQueryBuilder('c')
            ->where('c.parentCategory IS NULL')
        ;

        return $this->paginate($qb->getQuery(), $filter['page'], 100);
    }

    public function paginate($dql, int $page, int $limit = Article::COUNT_ON_PAGE): Paginator
    {
        $paginator = new Paginator($dql);
        $paginator->getQuery()
            ->setFirstResult((int) ($limit * ($page - 1)) < 0 ? 0 : $limit * ($page - 1))
            ->setMaxResults($limit)
        ;

        return $paginator;
    }
}
