<?php


namespace App\Controller;

use App\Entity\Article;
use App\Entity\Category;
use App\Form\Type\ArticleType;
use App\Form\Type\CategoryType;
use App\Service\ArticleService;
use App\Service\CategoryService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function article(Request $request, ArticleService $articleService, CategoryService $categoryService): Response {
        $categoryName = $request->query->get('category');
        $page = (is_numeric($request->query->get('page', 1)) && $request->query->get('page', 1) > 0) ? (int) $request->query->get('page', 1) : 1;

        switch ($request->query->get('sort' , 'desc')) {
            case 'asc':
                $sort = 'asc';
                break;
            case 'desc':
                $sort = 'desc';
                break;
            default:
                $sort = 'desc';
        }

        $articleFilter = [
            'page' => $page,
            'sort' => $sort,
            'category' => empty($categoryName) ? null : $categoryService->getOneBy(['title' => $categoryName]),
        ];

        $articles = $articleService->getForAdminPage($articleFilter);

        $maxPages = ceil(count($articles) / $articles->getQuery()->getMaxResults());

        return $this->render('admin/article.html.twig', [
            'thisPage' => $articleFilter['page'],
            'maxPages' => $maxPages,
            'articles' => $articles,
            'sort' => $sort,
        ]);
    }

    /**
     * @Route("/admin/categories", name="admin_categories")
     */
    public function category(Request $request, CategoryService $categoryService): Response {
        $page = (is_numeric($request->query->get('page', 1)) && $request->query->get('page', 1) > 0) ? (int) $request->query->get('page', 1) : 1;

        $categories = $categoryService->getForAdmin(['page' => $page]);

        $maxPages = ceil(count($categories) / $categories->getQuery()->getMaxResults());

        return $this->render('admin/category.html.twig', [
            'thisPage' => $page,
            'maxPages' => $maxPages,
            'categories' => $categories,
        ]);
    }

    /**
     * @Route("/admin/categories/{id}/remove", name="admin_categories_remove")
     */
    public function removeCategory(int $id, Request $request, CategoryService $categoryService): Response {
        $category = $categoryService->get($id);
        $categoryService->remove($category);

        return $this->redirectToRoute('admin_categories');
    }

    /**
     * @Route("/admin/categories/{id}/edit", name="admin_categories_edit")
     */
    public function editCategory(int $id, Request $request, CategoryService $categoryService): Response {
        $category = $categoryService->get($id);

        if (is_null($category)) {
            return new Response('Not Found', Response::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(CategoryType::class, $category);

        if ($form->handleRequest($request)->isSubmitted() && $form->isValid()) {
            $categoryService->create($category);

            return $this->redirectToRoute('admin_categories');
        }

        return $this->render('admin/edit_category.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/categories/create", name="admin_categories_create")
     */
    public function newCategory(Request $request, CategoryService $categoryService)
    {
        $category = new Category();

        $form = $this->createForm(CategoryType::class, $category);

        if ($form->handleRequest($request)->isSubmitted() && $form->isValid()) {
            $categoryService->create($category);

            return $this->redirectToRoute('admin_categories');
        }

        return $this->render('admin/new_category.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/articles/{id}/remove", name="admin_articles_remove")
     */
    public function removeArticle(int $id, Request $request, ArticleService $articleService): Response {
        $article = $articleService->get($id);
        $articleService->remove($article);

        return $this->redirectToRoute('admin');
    }

    /**
     * @Route("/admin/articles/{id}/edit", name="admin_articles_edit")
     */
    public function editArticle(int $id, Request $request, ArticleService $articleService): Response {
        $article = $articleService->get($id);

        if (is_null($article)) {
            return new Response('Not Found', Response::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(ArticleType::class, $article);

        if ($form->handleRequest($request)->isSubmitted() && $form->isValid()) {
            $articleService->create($article);

            return $this->redirectToRoute('admin');
        }

        return $this->render('admin/edit_article.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/articles/create", name="admin_articles_create")
     */
    public function newArticle(Request $request, ArticleService $articleService)
    {
        $article = new Article();
        $article->setCreatedAt(new \DateTime());

        $form = $this->createForm(ArticleType::class, $article);

        if ($form->handleRequest($request)->isSubmitted() && $form->isValid()) {
            $articleService->create($article);

            return $this->redirectToRoute('admin');
        }

        return $this->render('admin/new_article.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}