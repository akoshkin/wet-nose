<?php


namespace App\Controller;

use App\Service\ArticleService;
use App\Service\CategoryService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArticleController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(Request $request, ArticleService $articleService, CategoryService $categoryService): Response {
        $categoryName = $request->query->get('category');
        $page = (is_numeric($request->query->get('page', 1)) && $request->query->get('page', 1) > 0) ? (int) $request->query->get('page', 1) : 1;

        switch ($request->query->get('sort' , 'desc')) {
            case 'asc':
                $sort = 'asc';
                break;
            case 'desc':
                $sort = 'desc';
                break;
            default:
                $sort = 'desc';
        }

        $articleFilter = [
            'page' => $page,
            'sort' => $sort,
            'category' => empty($categoryName) ? null : $categoryService->getOneBy(['title' => $categoryName]),
        ];

        $articles = $articleService->getForHomePage($articleFilter);
        $categories = $categoryService->getMenu();

        $maxPages = ceil(count($articles) / $articles->getQuery()->getMaxResults());

        return $this->render('article/index.html.twig', [
            'thisPage' => $articleFilter['page'],
            'maxPages' => $maxPages,
            'articles' => $articles,
            'menu' => $categories,
            'sort' => $sort
        ]);
    }

    /**
     * @Route("/news/{slug}", name="show")
     */
    public function show(string $slug, ArticleService $articleService): Response {
        $article = $articleService->getOneBy(['slug' => $slug]);

        if (is_null($article)) {
            return new Response('Not Found', Response::HTTP_NOT_FOUND);
        } else {
            return $this->render('article/show.html.twig', [
                'article' => $article,
            ]);
        }
    }
}