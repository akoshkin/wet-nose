<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200322164900 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("
            CREATE TABLE category (
                id SERIAL PRIMARY KEY,
                title VARCHAR NOT NULL UNIQUE,
                parent_category_id INT REFERENCES category (id)
            );
        ");

        $this->addSql("
            CREATE TABLE article (
                id BIGSERIAL PRIMARY KEY,
                title VARCHAR NOT NULL,
                slug VARCHAR NOT NULL,
                preview TEXT NOT NULL,
                text TEXT NOT NULL,
                category_id INT NOT NULL REFERENCES category (id),
                status BOOLEAN NOT NULL DEFAULT TRUE,
                created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
            );
        ");
    }

    public function down(Schema $schema) : void
    {
        $this->addSql("DROP TABLE article;");
        $this->addSql("DROP TABLE category;");
    }
}
