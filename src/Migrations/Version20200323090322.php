<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200323090322 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            CREATE TABLE "user" (
                id SERIAL PRIMARY KEY,
                login VARCHAR NOT NULL UNIQUE,
                password VARCHAR NOT NULL,
                roles JSON NOT NULL
            );
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE "user"');
    }
}
