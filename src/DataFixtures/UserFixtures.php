<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\Category;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $this->loadUser($manager);
        $this->loadParentCategories($manager);
        $this->loadCategories($manager);
        $this->loadArticles($manager);
    }

    public function loadUser(ObjectManager $manager)
    {
        $user = new User();

        $user->setLogin('login');
        $user->setRoles(['ROLE_ADMIN']);
        $user->setPassword($this->passwordEncoder->encodePassword($user,'password'));

        $manager->persist($user);
        $manager->flush();
    }

    public function loadParentCategories(ObjectManager $manager)
    {
        foreach ($this->getParentCategoryData() as $name) {
            $category = new Category();
            $category->setTitle($name);

            $manager->persist($category);
            $this->addReference('category-'.$name, $category);
        }

        $manager->flush();
    }

    public function loadCategories(ObjectManager $manager)
    {
        foreach ($this->getCategoryData() as $key => $name) {
            $category = new Category();
            $category->setTitle($name);
            $category->setParentCategory($this->getReference('category-'. $this->getParentCategoryData()[$key]));

            $manager->persist($category);
            $this->addReference('category-'.$name, $category);
        }

        $manager->flush();
    }

    public function loadArticles(ObjectManager $manager)
    {
        foreach ($this->getArticleData() as [$title, $text, $createdAt, $preview, $category, $status]) {
            $article = new Article();
            $article->setTitle($title);
            $article->setText($text);
            $article->setCreatedAt($createdAt);
            $article->setStatus($status);
            $article->setPreview($preview);
            $article->setCategory($category);

            $manager->persist($article);
        }

        $manager->flush();
    }

    private function getCategoryData(): array
    {
        return [
            'JavaScript',
            'Ruby',
            'NodeJS',
            'JS',
            'MySQL',
        ];
    }

    private function getParentCategoryData(): array
    {
        return [
            'Symfony',
            'PHP',
            'Python',
            'HTML',
            'NoSQL',
        ];
    }

    private function getArticleData(): array
    {
        $statuses = Article::STATUSES_ALL;

        $articles = [];
        foreach ($this->getPhrases() as $title) {
            $randStatus = $statuses[array_rand($statuses)];

            $articles[] = [
                $title,
                $this->getRandomText(),
                new \DateTime('now + '.rand(1, 10) . ' minute'),
                $this->getRandomText(),
                $this->getRandomCategory(),
                $randStatus,
            ];
        }

        return $articles;
    }

    private function getRandomCategory()
    {
        $catNames = $this->getCategoryData();
        $catNames = array_merge($catNames, $this->getParentCategoryData());

        shuffle($catNames);

        return $this->getReference('category-'. $catNames[0]);
    }

    private function getPhrases(): array
    {
        return [
            'Lorem ipsum dolor sit amet consectetur adipiscing elit',
            'Pellentesque vitae velit ex',
            'Mauris dapibus risus quis suscipit vulputate',
            'Eros diam egestas libero eu vulputate risus',
            'In hac habitasse platea dictumst',
            'Morbi tempus commodo mattis',
            'Ut suscipit posuere justo at vulputate',
            'Ut eleifend mauris et risus ultrices egestas',
            'Aliquam sodales odio id eleifend tristique',
            'Urna nisl sollicitudin id varius orci quam id turpis',
            'Nulla porta lobortis ligula vel egestas',
            'Curabitur aliquam euismod dolor non ornare',
            'Sed varius a risus eget aliquam',
            'Nunc viverra elit ac laoreet suscipit',
            'Pellentesque et sapien pulvinar consectetur',
            'Ubi est barbatus nix',
            'Abnobas sunt hilotaes de placidus vita',
            'Ubi est audax amicitia',
            'Eposs sunt solems de superbus fortis',
            'Vae humani generis',
            'Diatrias tolerare tanquam noster caesium',
            'Teres talis saepe tractare de camerarius flavum sensorem',
            'Silva de secundus galatae demitto quadra',
            'Sunt accentores vitare salvus flavum parses',
            'Potus sensim ad ferox abnoba',
            'Sunt seculaes transferre talis camerarius fluctuies',
            'Era brevis ratione est',
            'Sunt torquises imitari velox mirabilis medicinaes',
            'Mineralis persuadere omnes finises desiderium',
            'Bassus fatalis classiss virtualiter transferre de flavum',
        ];
    }

    private function getRandomText(int $maxLength = 500): string
    {
        $phrases = $this->getPhrases();
        shuffle($phrases);

        while (mb_strlen($text = implode('. ', $phrases).'.') > $maxLength) {
            array_pop($phrases);
        }

        return $text;
    }
}
