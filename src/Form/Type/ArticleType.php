<?php

namespace App\Form\Type;


use App\Entity\Category;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, [
                        'attr' => [
                            'class' => 'form-control',
                            'placeholder' => 'title',
                        ],
                    ])
            ->add('preview', TextType::class, [
                        'attr' => [
                            'class' => 'form-control',
                            'placeholder' => 'preview',
                        ],
                    ])
            ->add('text', TextType::class, [
                        'attr' => [
                            'class' => 'form-control',
                            'placeholder' => 'text',
                        ],
                    ])
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'expanded' => false,
                'multiple' => false,
                'required' => true,
                'attr' => [
                    'class' => 'cat',
                    'data-placeholder' => 'category',
                ],
            ])
            ->add('status', CheckboxType::class)
            ->add('save', SubmitType::class)
        ;
    }
}