<?php

namespace App\Form\Type;


use App\Entity\Category;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class CategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                        'attr' => [
                            'class' => 'form-control',
                            'placeholder' => 'title',
                        ],
                    ])
            ->add('parentCategory', EntityType::class, [
                'class' => Category::class,
                'expanded' => false,
                'multiple' => false,
                'required' => false,
                'attr' => [
                    'class' => 'cat',
                    'data-placeholder' => 'parent category',
                ],
            ])
            ->add('save', SubmitType::class)
        ;
    }
}