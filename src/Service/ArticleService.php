<?php

namespace App\Service;


use App\Entity\Article;
use App\Repository\ArticleRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;

class ArticleService
{
    /**
     * @var ArticleRepository
     */
    private $articleRepository;

    /**
     * @var ObjectManager
     */
    private $manager;

    public function __construct(ArticleRepository $articleRepository, ManagerRegistry $manager)
    {
        $this->articleRepository = $articleRepository;
        $this->manager = $manager->getManager();
    }

    /**
     * @param array $params
     * @return Article[]
     */
    public function getBy(array $params) : array
    {
        return $this->articleRepository->findBy($params);
    }

    /**
     * @param array $params
     * @return Article|null
     */
    public function getOneBy(array $params) : ?Article
    {
        return $this->articleRepository->findOneBy($params);
    }

    public function getForHomePage(array $filter)
    {
        return $this->articleRepository->findForHomePage($filter);
    }

    public function getForAdminPage(array $filter)
    {
        return $this->articleRepository->findForAdminPage($filter);
    }

    public function get(int $id)
    {
        return $this->articleRepository->find($id);
    }

    /**
     * @return Article
     */
    public function remove(Article $article) : void
    {
        $this->manager->remove($article);
        $this->manager->flush();
    }

    /**
     * @return Article
     */
    public function create(Article $article) : void
    {
        $this->manager->persist($article);
        $this->manager->flush();
    }
}