<?php

namespace App\Service;


use App\Entity\Category;
use App\Repository\CategoryRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\Tools\Pagination\Paginator;

class CategoryService
{
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * @var ObjectManager
     */
    private $manager;

    public function __construct(CategoryRepository $categoryRepository, ManagerRegistry $manager)
    {
        $this->categoryRepository = $categoryRepository;
        $this->manager = $manager->getManager();
    }

    /**
     * @param array $params
     * @return Category[]
     */
    public function getBy(array $params) : array
    {
        return $this->categoryRepository->findBy($params);
    }

    /**
     * @return Category[]
     */
    public function getAll() : array
    {
        return $this->categoryRepository->findAll();
    }

    /**
     * @param array $params
     * @return Category
     */
    public function getOneBy(array $params) : ?Category
    {
        return $this->categoryRepository->findOneBy($params);
    }

    /**
     * @return array
     */
    public function getMenu() : array
    {
        return $this->categoryRepository->findBy(['parentCategory' => null]);
    }

    /**
     * @return Paginator
     */
    public function getForAdmin(array $filter) : Paginator
    {
        return $this->categoryRepository->findForAdminPage($filter);
    }

    /**
     * @return Category
     */
    public function remove(Category $category) : void
    {
        $this->manager->remove($category);
        $this->manager->flush();
    }

    /**
     * @return Category
     */
    public function create(Category $category) : void
    {
        $this->manager->persist($category);
        $this->manager->flush();
    }

    /**
     * @param int $id
     * @return Category
     */
    public function get(int $id) : ?Category
    {
        return $this->categoryRepository->find($id);
    }
}