# Include env files
include project.env
export

# VARIABLES
DC=docker-compose
D=docker
WORKDIR=-w /app
CONTAINER=${PROJECT_NAME}-php
CONTAINER_NODEJS=${PROJECT_NAME}-nodejs
CONTAINER_WEB=${PROJECT_NAME}-web
EXEC=$(D) exec -it $(WORKDIR)
CONSOLE=bin/console

# COMMANDS
.PHONY: up
up:
	$(DC) up -d

.PHONY: down
down:
	$(DC) stop

exec:
	$(EXEC) $(CONTAINER) bash

exec-web:
	$(EXEC) $(CONTAINER_WEB) bash

add-to-hosts:
	sudo sh -c 'echo "0.0.0.0 akoshkin.$(PROJECT_NAME).docker" >> /etc/hosts'

composer:
	$(EXEC) $(CONTAINER) composer install

db-init:
	$(EXEC) $(CONTAINER) $(CONSOLE) doctrine:database:create
	$(EXEC) $(CONTAINER) $(CONSOLE) doctrine:migrations:migrate --quiet

fixtures:
	$(EXEC) $(CONTAINER) $(CONSOLE) doctrine:fixtures:load --quiet

migrate:
	$(EXEC) $(CONTAINER) $(CONSOLE) doctrine:migrations:migrate --quiet

yarn:
	$(EXEC) $(CONTAINER_NODEJS) yarn install

yarn-run:
	$(EXEC) $(CONTAINER_NODEJS) yarn run encore dev

first-start: up composer db-init fixtures